# --> define the Xml headers
head0='<?xml version="1.0" encoding="ISO-8859-1"?>'
head1='<!DOCTYPE DDDB SYSTEM "git:/DTD/structure.dtd">'

# --> DDDB tag9999
DDDBTag="<DDDB>"
endDDDBTag="</DDDB>"

# --> Author
Author = "<!-- Athor: Jianchun Wang, Wojciech Krupa-->"

# change it later to loop over different catalog 
# --> catalog name
catalogName='<catalog name="NoiseValues">'

# --> open the Xml file and write out the data
XmlFileName='./UT.xml'
XmlFile=open(XmlFileName, 'w')

# --> condition tag
condTag='<condition classID="5" name="'
endCondTag='</condition>'


# --> condition value types
intType=' type="int">'
doubleType=' type="double">'
stringType=' type="string">'

# --> condition tags
condANumberTag='<param name='
condAVecTag='<paramVector name='
endANumberTag='</param>'
endAVecTag='</paramVector>'
# --> TAG definitions --

# --> condition names
SectorNoise ='"SectorNoise"'
cmNois ='"cmNois"'
electronsPerADC ='"electronsPerADC"'

condNamesAVector=[SectorNoise, cmNois, electronsPerADC  ]

# --> SectorNoise values
init=1.50
SectorNoise_val=str()
for n in range(512):
 SectorNoise_val+=(' '+str(init))

# --> cmNois values
init=1.00
cmNois_val=str()
for n in range(512):
 cmNois_val+=(' '+str(init))

# --> electronsPerADC values
init=1000
electronsPerADC_val=str()
for n in range(512):
 electronsPerADC_val+=(' '+str(init))


# --> list of conditions values

condValuesAVector=[SectorNoise_val, cmNois_val, electronsPerADC_val]

# --> UT layer
UTLAYERS = ["aX", "aU", "bV", "bX"]

# --> UT Layer Region
REGION=["R1", "R2", "R3"]

# --> UT Layer Sectors
SECTORS=[1, 2, 3, 4, 5, 6, 7, 8,9 ,10, 11, 12, 13, 14]

# --> UT Stave
STAVES=[1, 2, 3, 4, 5, 6]

# ------------------------------------
# --> creating the Xml file's content
# ------------------------------------
# --> Header of the Xml
print >> XmlFile, head0
print >> XmlFile, head1
# --> <DDDB>
print >> XmlFile, DDDBTag
print >> XmlFile, catalogName
print >> XmlFile, Author
print >> XmlFile

# --> Layer conditions -- loop UT Layers
for layer in UTLAYERS:
 for region in REGION:
   for stave in STAVES:
    for sector in SECTORS:

      # --> Condition collection Tag
      # --> dynamic name of the condition
      # --> <condition ... >
      condTarget= "UT" + layer + "Layer" + region + "Stave"+ str(stave)+"Sector"+ str(sector)+'">'
      print >> XmlFile, condTag+condTarget
      # --> conditions - single number first -- loop 1
      for n in range(len(condNamesAVector)):
        print >> XmlFile, condAVecTag+condNamesAVector[n]+doubleType,condValuesAVector[n], endAVecTag
      # --> </condition>
      print >> XmlFile, endCondTag
      # --> </DDDB>
      print >> XmlFile

print >> XmlFile, endDDDBTag
 
# --> close the Xml file
XmlFile.close()
