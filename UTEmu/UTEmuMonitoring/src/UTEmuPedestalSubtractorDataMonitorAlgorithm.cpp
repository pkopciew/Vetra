/*
 * UTEmuPedestalDataMonitorAlgorithm.cpp
 *
 *  Created on: Oct 18, 2014
 *      Author: ADendek
 */

#include "UTEmuPedestalSubtractorDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"
#include "UTEmu/UTEmuDataType.h"
#include "GaudiUtils/Aida2ROOT.h"

DECLARE_COMPONENT(UTEmu::PedestalSubtractorDataMonitorAlgorithm)

using namespace UTEmu;

PedestalSubtractorDataMonitorAlgorithm::PedestalSubtractorDataMonitorAlgorithm(
    const std::string &name, ISvcLocator *pSvcLocator)
    : DataMonitorAlgorithm(name, pSvcLocator)
{
  m_input = UTEmu::DataLocations::PedestalTES;
}

std::string PedestalSubtractorDataMonitorAlgorithm::createHistogramName(unsigned int asic_num)
{
  return "Data_after_pedestal_event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

TH2D *PedestalSubtractorDataMonitorAlgorithm::bookHistogram2D(
    const std::string &p_histogramName, const std::string &p_histogramTitle,
    int p_sensorNumber)
{
  int l_ylow = -35;
  int l_yhigh = 35;
  int l_ybin = 70;
  return Gaudi::Utils::Aida2ROOT::aida2root(book2D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() - 0.5, p_sensorNumber, l_ylow, l_yhigh,
      l_ybin));
}

std::string PedestalSubtractorDataMonitorAlgorithm::createHistogramTitle(unsigned int asic_num)
{
  return "Data after Pedestal Subtraction - event" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num );
}

void PedestalSubtractorDataMonitorAlgorithm::createHistogram2D()
{
  for (int asic_num = 0; asic_num < m_numOfASICs; asic_num++)
  {
    std::string l_histogramName = "PedestalData_vs_channel_" + std::to_string(asic_num );
    std::string l_histogramTtttle = "Data after Pedestal vs channel_" + std::to_string(asic_num );
    int l_sensorNum = RawData<>::getnChannelNumber();
    m_histogram2D[asic_num] =
        bookHistogram2D(l_histogramName, l_histogramTtttle, l_sensorNum);
  }
}

StatusCode PedestalSubtractorDataMonitorAlgorithm::finalize()
{
  DataMonitorAlgorithm::m_outpuProjectionHistogramName = "ProjectionPedestal";
  return DataMonitorAlgorithm::finalize();
}
