/*
 * UTEmuCommonModeSubtractorDataMonitorAlgorithm.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: ADendek
 */

#include "UTEmuCommonModeSubtractorDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"
#include "UTEmu/UTEmuDataType.h"
#include "GaudiUtils/Aida2ROOT.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::CommonModeSubtractorDataMonitorAlgorithm)

using namespace UTEmu;

CommonModeSubtractorDataMonitorAlgorithm::
    CommonModeSubtractorDataMonitorAlgorithm(const std::string &name,
                                             ISvcLocator *pSvcLocator)
    : DataMonitorAlgorithm(name, pSvcLocator)
{
  m_input = UTEmu::DataLocations::CMSTES;
  m_noise = std::vector<Noise>(m_numOfASICs);
  m_noisePerChannelHistograms = std::vector<HistogramMap>(m_numOfASICs);
}
StatusCode CommonModeSubtractorDataMonitorAlgorithm::execute()
{
  DataMonitorAlgorithm::RunPhase l_runPhase =
      DataMonitorAlgorithm::getRunPhase();
  switch (l_runPhase)
  {
  case SKIP:
    return DataMonitorAlgorithm::skippEvent();
  case SAVE_SINGLE_EVENTS:
    return saveSimpleEvents();
  default:
    return fillOnly2DHistogram();
  }
}

StatusCode CommonModeSubtractorDataMonitorAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawDataMap<double>>(input_loc);
  if (!m_dataMap)
  {
    error() << "=> there is no input data in " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

StatusCode CommonModeSubtractorDataMonitorAlgorithm::saveSimpleEvents()
{
  setHistoDir(const_cast<char *>("UTEmu__CommonModeSubtractorDataMonitor_Events/"));

  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawData<double>(Data);
    storeEventIntoHistogram(asic_num);
    fillHistogram2D(asic_num);
  }

  m_evtNumber++;
  return StatusCode::SUCCESS;
}

TH2D *CommonModeSubtractorDataMonitorAlgorithm::bookHistogram2D(
    const std::string &p_histogramName, const std::string &p_histogramTitle,
    int p_sensorNumber)
{
  int l_ylow = -35;
  int l_yhigh = 35;
  int l_ybin = 70;
  return Gaudi::Utils::Aida2ROOT::aida2root(book2D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() - 0.5, 128, l_ylow, l_yhigh,
      l_ybin));
}

std::string CommonModeSubtractorDataMonitorAlgorithm::createHistogramTitle(unsigned int asic_num)
{
  return "Data after CM Subtraction - event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num);
}

std::string CommonModeSubtractorDataMonitorAlgorithm::createHistogramName(unsigned int asic_num)
{
  return "Data_after_CMS_event_" + std::to_string(m_evtNumber) + "_" + std::to_string(asic_num);
}

void CommonModeSubtractorDataMonitorAlgorithm::createHistogram2D()
{
  for (unsigned int asic_num = 0; asic_num < m_numOfASICs; asic_num++)
  {
    std::string l_histogramName = "CMSData_vs_channell_" + std::to_string(asic_num);
    std::string l_histogramTtttle = "Data after CMS vs channell_" + std::to_string(asic_num);
    int l_sensorNum = RawData<>::getnChannelNumber();
    m_histogram2D[asic_num] =
        bookHistogram2D(l_histogramName, l_histogramTtttle, l_sensorNum);
    if (m_debug)
      createNoiseHistograms(asic_num);
  }
}

StatusCode CommonModeSubtractorDataMonitorAlgorithm::fillOnly2DHistogram()
{
  setHistoDir(const_cast<char *>("UTEmu__CommonModeSubtractorDataMonitor/"));

  if (StatusCode::SUCCESS != getData(m_input))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
    return StatusCode::SUCCESS;
  const int storeNoiseFrequency = 700;
  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = RawData<double>(Data);
    m_noise[asic_num].updateNoise(&(m_data));
    if (DataMonitorAlgorithm::m_evtNumber % storeNoiseFrequency == 0 && m_debug) //should be adjusted
    {
      fillNoiseHistograms(asic_num);
    }
    fillHistogram2D(asic_num);
  }

  DataMonitorAlgorithm::m_evtNumber++;
  return StatusCode::SUCCESS;
}

void CommonModeSubtractorDataMonitorAlgorithm::fillHistogram2D(unsigned int asic_num)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    auto channelSignal = m_data.getSignal(chan);
    if (0 != channelSignal /* && m_data.getNum(chan) == asic_num*/) // no need to push masked values //check that!
      m_histogram2D[asic_num]->Fill(chan + RawData<>::getMinChannel(), channelSignal);
  }
}

void CommonModeSubtractorDataMonitorAlgorithm::fillHistogram(
    TH1D *p_histogram)
{
  int channelNumber = RawData<>::getnChannelNumber();
  for (int chan = 0; chan < channelNumber; chan++)
  {
    p_histogram->SetBinContent(chan, m_data.getSignal(chan));
  }
}

StatusCode CommonModeSubtractorDataMonitorAlgorithm::finalize()
{
  DataMonitorAlgorithm::m_outpuProjectionHistogramName = "ProjectionCommonMode";
  return DataMonitorAlgorithm::finalize();
}

void CommonModeSubtractorDataMonitorAlgorithm::createNoiseHistograms(unsigned int asic_num)
{
  const int l_channelNumber = RawData<>::getnChannelNumber();
  const int l_ylow = 0;
  const int l_yhigh = 32;
  const int l_ybin = 32;
  const int l_xlow = 0;
  const int l_xhigh = 5;

  for (int channel = 0; channel < l_channelNumber; channel++)
  {
    std::string l_histogramName = "Noise_channel_" + std::to_string(asic_num) + "_" + std::to_string(channel);
    m_noisePerChannelHistograms[asic_num].insert(std::make_pair(
        channel, Gaudi::Utils::Aida2ROOT::aida2root(
                     book1D(l_histogramName, l_histogramName,
                            l_ybin, l_ylow, l_yhigh))));
    if (0 == channel)
    {
      l_histogramName = "Noise_SALT_" + std::to_string(asic_num);
      m_noisePerSensorHistograms.insert(std::make_pair(
          asic_num, Gaudi::Utils::Aida2ROOT::aida2root(
                        book1D(l_histogramName, l_histogramName,0,5))));    }
  }
}

void CommonModeSubtractorDataMonitorAlgorithm::fillNoiseHistograms(unsigned int asic_num)
{
  m_noise[asic_num].NormalizeNoise();

  for (auto noiseHistogramIt : m_noisePerChannelHistograms[asic_num])
  {
    noiseHistogramIt.second->Fill(m_noise[asic_num].getNoise(noiseHistogramIt.first));
    m_noisePerSensorHistograms[asic_num]->Fill(m_noise[asic_num].getNoise(noiseHistogramIt.first));
  }
}
