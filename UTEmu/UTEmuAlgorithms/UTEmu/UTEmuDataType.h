#pragma once
#include "GaudiKernel/DataObject.h"
#include <string>
#include <vector>
#include <iostream>
#include "UTEmu/UTEmuDataLocations.h"
#include "UTEmu/UTEmuSensor.h"
#include <map>
namespace UTEmu
{
template <typename DATA_TYPE = int>
class RawData
{
public:
  typedef std::vector<DATA_TYPE> SignalVector;
  typedef DATA_TYPE DataType;

  static void setSensor(Sensor p_sensor) { s_sensor = p_sensor; }
  static int getMaxChannel() { return s_sensor.maxChannel(); }
  static int getMinChannel() { return s_sensor.minChannel(); }
  static int getnChannelNumber()
  {
    return s_sensor.channelNumber * s_sensor.sensorNumber();
  }

  static int getSensorNumber() { return s_sensor.sensorNumber(); }

  RawData<DATA_TYPE>()
      : m_signal(),
        m_header0(),
        m_header1(),
        m_header2(),
        m_header3(),
        m_time(),
        m_tdc(),
        m_valid(){};

  RawData<DATA_TYPE> &operator=(const RawData<DATA_TYPE> &rhs)
  {
    if (this != &rhs)
    {
      m_signal = rhs.m_signal;
      m_header0 = rhs.m_header0;
      m_header1 = rhs.m_header1;
      m_header2 = rhs.m_header2;
      m_header3 = rhs.m_header3;
      m_num = rhs.m_num;
      m_time = rhs.m_time;
      m_tdc = rhs.m_tdc;
      m_valid = rhs.m_valid;
    }
    return *this;
  }
  RawData<DATA_TYPE>(const RawData<DATA_TYPE> &rhs)
      : m_signal(rhs.m_signal),
        m_header0(rhs.m_header0),
        m_header1(rhs.m_header1),
        m_header2(rhs.m_header2),
        m_header3(rhs.m_header3),
        m_num(rhs.m_num),
        m_time(rhs.m_time),
        m_tdc(rhs.m_tdc),
        m_valid(rhs.m_valid) {}

  void setValid(bool p_valid) { m_valid = p_valid; }
  void setTime(unsigned long long p_time) { m_time = p_time; }
  void setSignal(DATA_TYPE p_signal) { m_signal.push_back(p_signal); }
  void setHeader0(DATA_TYPE p_header) { m_header0.push_back(p_header); }
  void setHeader1(DATA_TYPE p_header) { m_header1.push_back(p_header); }
  void setHeader2(DATA_TYPE p_header) { m_header2.push_back(p_header); }
  void setHeader3(DATA_TYPE p_header) { m_header3.push_back(p_header); }
  void setNum(DATA_TYPE p_num) { m_num.push_back(p_num); }
  SignalVector &getSignal() { return m_signal; }
  SignalVector &getHeader0() { return m_header0; }
  SignalVector &getHeader1() { return m_header1; }
  SignalVector &getHeader2() { return m_header2; }
  SignalVector &getHeader3() { return m_header3; }
  SignalVector &getNum() { return m_num; }

  unsigned long long getTime() const { return m_time; }
  bool getValid() const { return m_valid; }

  DATA_TYPE getSignal(int channel) const { return m_signal[channel]; }

  DATA_TYPE getHeader0(int subset) const { return m_header0[subset]; }
  DATA_TYPE getHeader1(int subset) const { return m_header1[subset]; }
  DATA_TYPE getHeader2(int subset) const { return m_header2[subset]; }
  DATA_TYPE getHeader3(int subset) const { return m_header3[subset]; }
  DATA_TYPE getNum(int subset) const { return m_num[subset]; }

  unsigned int getTDC() const { return m_tdc; }
  void setTDC(unsigned int p_tdc) { m_tdc = p_tdc; }

protected:
  SignalVector m_signal;
  SignalVector m_header0;
  SignalVector m_header1;
  SignalVector m_header2;
  SignalVector m_header3;
  SignalVector m_num;
  unsigned long long m_time;
  unsigned int m_tdc;
  bool m_valid;

  static Sensor s_sensor;
};

template <typename DataType = int>
class RawDataContainer : public DataObject
{
public:
  typedef std::vector<RawData<DataType>> RawDataVec;

  RawDataContainer<DataType>() : empty(true), m_dataVector(){};

  void addData(RawData<DataType> rawData)
  {
    m_dataVector.push_back(rawData);
    empty = false;
  }
  bool isEmpty() { return empty; }
  RawDataVec getData() { return m_dataVector; }

private:
  bool empty;
  RawDataVec m_dataVector;
};

template <typename DataType = int>
class RawDataMap : public DataObject
{
public:
  typedef std::map<int, RawData<DataType>> RawDataMp;

  RawDataMap<DataType>() : empty(true), m_dataMap(){};

  void addData(int asic_num, RawData<DataType> rawData)
  {
    m_dataMap.insert({asic_num, rawData});
    empty = false;
  }
  bool isEmpty() { return empty; }
  RawDataMp getData() { return m_dataMap; }
  unsigned int Size() { return m_dataMap.size(); }
private:
  bool empty;
  RawDataMp m_dataMap;
};


class AsicMap : public DataObject
{
public:
  typedef std::map<unsigned int, bool> AsicMp;

  AsicMap() : empty(true), m_asicMap(){};

  void addAsic( unsigned int asic_num, bool isActive)
  {
    m_asicMap.insert({asic_num, isActive});
    empty = false;
  }
  bool isEmpty() { return empty; }

  AsicMp getAsicMap() { return m_asicMap; }
  bool operator [] (unsigned int asic ) { return m_asicMap[asic]; } 
  unsigned int Size() { return m_asicMap.size(); }
private:
  bool empty;
  AsicMp m_asicMap;
};


class RawZSData
{
public:

  typedef std::map<unsigned int, double> Signal;

  RawZSData(): 
        m_signal(),
        m_valid(){};

  RawZSData &operator=(const RawZSData &rhs)
  {
    if (this != &rhs)
    {
      m_signal = rhs.m_signal;
      m_valid = rhs.m_valid;
    }
    return *this;
  }

  RawZSData(const RawZSData &rhs)
      : m_signal(rhs.m_signal),
        m_valid(rhs.m_valid) {}

  void setValid(bool p_valid) { m_valid = p_valid; }
  void setSignal(unsigned int p_channel, double p_signal) { m_signal.insert({p_channel,p_signal}); }
  Signal &getSignal() { return m_signal; }
  bool getValid() const { return m_valid; }

  double getSignal(unsigned int channel){ return m_signal[channel]; }

protected:
  Signal m_signal;
  bool m_valid;

};

class RawZSDataMap : public DataObject
{
public:
  typedef std::map<int, RawZSData> RawZSDataMp;

  RawZSDataMap() : empty(true), m_dataMap(){};

  void addData(int asic_num, RawZSData rawData)
  {
    m_dataMap.insert({asic_num, rawData});
    empty = false;
  }
  bool isEmpty() { return empty; }
  RawZSDataMp getData() { return m_dataMap; }
  unsigned int Size() { return m_dataMap.size(); }
private:
  bool empty;
  RawZSDataMp m_dataMap;
};



} // namespace UTEmu


