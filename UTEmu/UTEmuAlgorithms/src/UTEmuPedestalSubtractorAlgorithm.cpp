/*
 * UTEmuPedestalSubtractorAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#include "UTEmuPedestalSubtractorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::PedestalSubtractorAlgorithm)

PedestalSubtractorAlgorithm::PedestalSubtractorAlgorithm(
    const std::string &name, ISvcLocator *pSvcLocator)
    : GaudiAlgorithm(name, pSvcLocator),
      m_isStandalone(true),
      m_data(),
      m_PedestalTES(),
      m_channelMaskInputLocation(),
      m_followingOption(),
      m_event(0),
      m_treningEventNumber(512),
      m_skippEvent(10),
      m_numofAsics(),
      m_debug(),
      m_channelMaskFileValidator(),
      m_channelMaskProvider()
{
  declareProperty("PedestalTESLocation", m_PedestalTES = UTEmu::DataLocations::PedestalTES);
  declareProperty("SkippEventNumber", m_skippEvent = 0);
  declareProperty(
      "InputDataLocation",
      m_inputDataLocation = UTEmu::DataLocations::RawTES);
  declareProperty("FollowingOption",
                  m_followingOption = UTEmu::FollowingOptions::Calculator);
  declareProperty("treningEntry", m_treningEventNumber = 512);
  declareProperty(
      "ChannelMaskInputLocation",
      m_channelMaskInputLocation = UTEmu::DataLocations::MaskLocation);
  declareProperty("standalone", m_isStandalone = true);
  declareProperty("NumberofAsics", m_numofAsics = UTEmu::DataLocations::number_of_asics);
  declareProperty("Debug", m_debug = false);
}

StatusCode PedestalSubtractorAlgorithm::initialize()
{
  if (StatusCode::SUCCESS != initializeBase())
    return StatusCode::FAILURE;
  if (StatusCode::SUCCESS != buildFollowing())
    return StatusCode::FAILURE;
  if (StatusCode::SUCCESS != retriveMasksFromFile())
    return StatusCode::FAILURE;

  info() << "Initialized successfully!" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode PedestalSubtractorAlgorithm::execute()
{
  m_outputDataMap = new RawDataMap<>();

  if (StatusCode::SUCCESS != getData(m_inputDataLocation))
    return StatusCode::FAILURE;
  if (m_dataMap->isEmpty())
  {
    info() << "ped suba put empty" << endmsg;
    put(m_outputDataMap, m_PedestalTES);
    return StatusCode::SUCCESS;
  }

  for (const auto &[asic_num, Data] : m_dataMap->getData())
  {
    m_data = new RawData<>(Data);
    RunPhase l_runPhase = getRunPhase();
    switch (l_runPhase)
    {
    case SKIPP:
      skippEvent();
      break;
    case TREANING:
      processTreaning(asic_num);
      break;
    default:
      subtractPedestals(asic_num);
    }
    delete m_data;
  }

  m_event++;
  put(m_outputDataMap, m_PedestalTES);
  return StatusCode::SUCCESS;
}

StatusCode PedestalSubtractorAlgorithm::finalize()
{
  savePedestalsToFile();
  return GaudiAlgorithm::finalize();
}

StatusCode PedestalSubtractorAlgorithm::initializeBase()
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto channel_maskValidator = ChannelMaskFileValidator(m_channelMaskInputLocation);
    m_channelMaskFileValidator.push_back(channel_maskValidator);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto channel_maskProvider = ChannelMaskProvider(m_channelMaskFileValidator[asic_num]);
    m_channelMaskProvider.push_back(channel_maskProvider);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto pedestal = Pedestal();
    m_pedestals.push_back(pedestal);

    auto pedestal_following = PedestalFollowingPtr();
    m_pedestalFollowingPtrs.push_back(pedestal_following);

    auto pedestal_location = UTEmu::DataLocations::UTEmu_path + "UTEmu/UTEmuOptions/options/UT/Pedestal_ASIC" + std::to_string(asic_num) + ".dat";
    PedestalLocations.push_back(pedestal_location);
  }

  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto pedestal_validator = PedestalFileValidator(PedestalLocations[asic_num]);
    m_pedestalFileValidators.push_back(pedestal_validator);
  }
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    auto following_factory = PedestalFollowingFactory(m_channelMaskProvider[asic_num], m_pedestals[asic_num], m_pedestalFileValidators[asic_num], PedestalLocations[asic_num], m_debug);
    m_followingFactories.push_back(following_factory);

    auto pedestal_subtractor = PedestalSubtractor(m_pedestals[asic_num], m_channelMaskProvider[asic_num]);
    m_pedestalSubtractors.push_back(pedestal_subtractor);
  }

  return GaudiAlgorithm::initialize();
}

StatusCode PedestalSubtractorAlgorithm::buildFollowing()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
  {
    m_pedestalFollowingPtrs[asic_num].reset(
        m_followingFactories[asic_num].createPedestalFollowing(m_followingOption));
  }
  return StatusCode::SUCCESS;
}
catch (PedestalFollowingFactory::NoSuchState &exception)
{
  error() << "Invalid Following Option: " << exception.what() << endmsg;
  return StatusCode::FAILURE;
}

StatusCode PedestalSubtractorAlgorithm::retriveMasksFromFile()
try
{
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    (m_channelMaskProvider[asic_num]).getMaskFromFile(m_channelMaskInputLocation);
  return StatusCode::SUCCESS;
}
catch (ChannelMaskProvider::InputFileError &exception)
{
  error() << "Channel Mask File Input Error: " << m_channelMaskInputLocation
          << endmsg;
  return StatusCode::FAILURE;
}

StatusCode PedestalSubtractorAlgorithm::getData(std::string input_loc)
{
  m_dataMap = getIfExists<RawDataMap<>>(input_loc);
  if (!m_dataMap)
  {
    error() << " ==> There is no input data: " << input_loc << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

PedestalSubtractorAlgorithm::RunPhase
PedestalSubtractorAlgorithm::getRunPhase()
{
  if (m_event < m_skippEvent)
    return SKIPP;
  else if (m_event < m_skippEvent + m_treningEventNumber)
    return TREANING;
  else
    return SUBTRACTION;
}

void PedestalSubtractorAlgorithm::skippEvent() {}

void PedestalSubtractorAlgorithm::processTreaning(unsigned int asic_num)
{
  m_pedestalFollowingPtrs[asic_num]->processEvent(m_data);
}

void PedestalSubtractorAlgorithm::subtractPedestals(unsigned int asic_num)
{
  processAndSaveDataToTES(asic_num);
}

void PedestalSubtractorAlgorithm::processAndSaveDataToTES(unsigned int asic_num)
{
  RawData<> *afterPedestal = new RawData<>();
  m_pedestalSubtractors[asic_num].processEvent(m_data, &afterPedestal);
  m_outputDataMap->addData(asic_num, *afterPedestal);
}

StatusCode PedestalSubtractorAlgorithm::savePedestalsToFile()
try
{
  info() << "Save Pedestal to File" << endmsg;
  for (unsigned int asic_num = 0; asic_num < m_numofAsics; asic_num++)
    m_pedestalFollowingPtrs[asic_num]->savePedestalToFile(PedestalLocations[asic_num]);
  return StatusCode::SUCCESS;
}
catch (IPedestalFollowing::PedestalCalculatorError &er)
{
  error() << er.what() << endmsg;
  return StatusCode::FAILURE;
}
