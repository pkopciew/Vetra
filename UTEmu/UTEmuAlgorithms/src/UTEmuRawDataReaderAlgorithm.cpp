/*
 *  UTEmuRawDataReaderAlgorithm.cpp
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */

#include "UTEmuRawDataReaderAlgorithm.h"
#include "GaudiKernel/IEventProcessor.h"

using namespace UTEmu;

DECLARE_COMPONENT(UTEmu::RawDataReaderAlgorithm)

RawDataReaderAlgorithm::RawDataReaderAlgorithm(const std::string &name,
                                               ISvcLocator *pSvcLocator)
    : GaudiAlgorithm(name, pSvcLocator),
      m_isStandalone(true),
      m_eventNumber(0),
      m_skipEventNumber(0),
      m_InputData(),
      m_outputLocation(),
      m_sensorNumber(1),
      m_numOfASICs(0),
      m_mean(0),
      m_sigma(1),
      m_fileValidator(m_InputData),
      m_rawDataReader(),
      m_inputDataFactory(m_InputData, m_fileValidator,
                         m_numOfASICs, m_mean, m_sigma)
{
  declareProperty("InputDataType",
                  m_inputDataOption = UTEmu::InputDataOption::Binary);
  declareProperty("SkipEventNumber", m_skipEventNumber = 0);
  declareProperty("inputData", m_InputData = "");
  declareProperty("outputDataLocation", m_outputLocation = UTEmu::DataLocations::RawTES);
  declareProperty("sensorNumber", m_sensorNumber = 1);
  declareProperty("standalone", m_isStandalone = true);
  declareProperty("sigma", m_sigma = 0.);
  declareProperty("mean", m_mean = 1.);
  declareProperty("numOfASICs", m_numOfASICs = 4);
}

StatusCode RawDataReaderAlgorithm::initialize()
{
  StatusCode sc = GaudiAlgorithm::initialize();
  if (!sc.isSuccess())
    return StatusCode::FAILURE;
  try
  {
    m_rawDataReader = m_inputDataFactory.createDataEngine(m_inputDataOption);
    info() << "Create data engine done" << endmsg;
  }
  catch (RawDataFactory::NoSuchState &ex)
  {
    error() << "Error input in factory: " << ex.what() << endmsg;
    return StatusCode::FAILURE;
  }
  try
  {
    m_rawDataReader->checkInput();
    return StatusCode::SUCCESS;
  }
  catch (IDataReader::InputFileError &ex)
  {
    error() << "input file error: " << ex.what() << endmsg;
    return StatusCode::FAILURE;
  }
  try
  {
    Sensor l_sensor(m_sensorNumber);
    RawData<>::setSensor(l_sensor);
  }
  catch (Sensor::SensorNumberError &ex)
  {
    error() << "Error create sensor: " << ex.what() << endmsg;
    return StatusCode::FAILURE;
  }
  info() << "initialization success!" << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode RawDataReaderAlgorithm::execute()
{
  RawDataMap<> *outputDataMap = new RawDataMap();
  try
  {
    if(m_eventNumber % 100 == 0)
    info() << "Read event" << m_eventNumber << endmsg;
    std::array<RawData<>, 4> *outputData = m_rawDataReader->getEventData();

    for (unsigned int i = 0; i < UTEmu::DataLocations::number_of_asics-1; i++)
      outputDataMap->addData(i, (*outputData)[i]);
    outputDataMap->addData(4, (*outputData)[0]);
    m_eventNumber++;
  }
  catch (IDataReader::ReadEventError &ex)
  {
    error() << "event read error: " << ex.what() << endmsg;
    return StatusCode::RECOVERABLE;
  }
  catch (IDataReader::NoMoreEvents &ex)
  {
    SmartIF<IEventProcessor> app(serviceLocator()->service("ApplicationMgr"));
    if (app)
    {
      info() << "No more event. Terminate!" << endmsg;
      return app->stopRun();
    }
  }
  put(outputDataMap, m_outputLocation);
  return StatusCode::SUCCESS;
}
