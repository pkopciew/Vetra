//
// Created by ja on 8/4/15.
//

#include "UTEmuCmsPerSALT.h"
#include <iostream>
#include <cmath>

using namespace UTEmu;
using namespace std;

CmsPerSALT::CmsPerSALT(IChannelMaskProvider& p_masksProvider,
                           double p_hitThreshold)
    : m_masksProvider(p_masksProvider),
      m_channelNumber(RawData<>::getnChannelNumber()),
      m_hitThreshold(p_hitThreshold) {
  cout << "create correlation per SALT" << endl;
  initializeCorrectionMap();
}

void CmsPerSALT::initializeCorrectionMap() {
  int channelPerSALT = 128;
  for (int channel = 0; channel < m_channelNumber; channel += channelPerSALT)
    m_correctionPerSALT.insert(std::make_pair(channel, 0));
}

void CmsPerSALT::processEvent(RawData<>* p_data, RawData<double>** p_output) {
  calculateCorrection(p_data);
  removeCM(p_data, p_output);
}

void CmsPerSALT::calculateCorrection(RawData<>* p_inputData) {
  int channelPerSALT = 128;
  for (auto& mapIt : m_correctionPerSALT) {
    int usedChannels = 0;
    for (int channel = mapIt.first; channel < mapIt.first + channelPerSALT;
         channel++) {
      auto signal = p_inputData->getSignal(channel);
      if (!m_masksProvider.isMasked(channel) && abs(signal) < m_hitThreshold) {
        mapIt.second += signal;
        usedChannels++;
      }
    }
    if (usedChannels) mapIt.second /= static_cast<double>(usedChannels);
  }
}

void CmsPerSALT::removeCM(RawData<>* p_data, RawData<double>** p_output) {
  int channelPerSALT = 128;
  for (auto& mapIt : m_correctionPerSALT) {
    for (int channel = mapIt.first; channel < mapIt.first + channelPerSALT;
         channel++) {
      if (m_masksProvider.isMasked(channel)) {
        double signalMaskedChannel = 0;
        (*p_output)->setSignal(signalMaskedChannel);
      } else {
        double l_channelSignal = p_data->getSignal(channel) - mapIt.second;
        (*p_output)->setSignal(l_channelSignal);
      }
    }
  }
}
