/*
 * UTEmuCommonModeSubtractorAlgorithm.h
 *
 *  Created on: Oct 14, 2014 
 *  Uptaded on: March, 2020 
 *      Author: ADendek
 *      Author: WKrupa (wokrupa@cern.ch)
 */
#pragma once

#include "GaudiAlg/GaudiAlgorithm.h"

#include "UTEmu/UTEmuDataType.h"
#include "UTEmuCommonModeSubtractorFactory.h"
#include "UTEmuChannelMaskProvider.h"
#include "UTEmuChannelMaskFileValidator.h"
#include "UTEmuNoiseCalculatorfactory.h"
#include "UTEmu/UTEmuNoise.h"
#include <memory.h>
namespace UTEmu
{

class CommonModeSubtractorAlgorithm : public GaudiAlgorithm
{
  typedef std::shared_ptr<ICommonModeSubtractor> CMSubtractorPtr;

public:
  CommonModeSubtractorAlgorithm(const std::string &name,
                                ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

private:
  StatusCode initializeBase();
  StatusCode buildSubtractorEngine();
  StatusCode buildNoiseCalculator();
  StatusCode retriveMasksFromFile();

  StatusCode getData(std::string);
  void processEvent(int asic_num);
  StatusCode skippTreaningEvent();

  StatusCode saveNoiseToFile();

  RawDataMap<> *m_dataMap;
  RawData<> *m_data;
  RawDataMap<double> *m_outputDataMap;

  std::string m_inputDataLocation;
  std::string m_outputDataLocation;

  std::string m_channelMaskInputLocation;
  std::string m_CMSOption;
  std::string m_noiseCalculatorType;
  std::vector<std::string> m_noiseOutputLocations;
  std::vector<std::string> m_noiseBeforeCMSOutputLocations;
  int m_event;
  int m_skipEvent;
  unsigned int m_numofAsics;

  std::vector<ChannelMaskFileValidator> m_channelMaskFileValidator;
  std::vector<ChannelMaskProvider> m_channelMaskProvider;
  std::vector<CommonModeSubtractorFactory> m_SubtractorFactories;
  std::vector<NoiseCalculatorFactory> m_noiseCalculatorFactories;
  std::vector<NoiseCalculatorFactory> m_noise_beforeCMS_CalculatorFactories; 
  std::vector<NoiseCalculatorFactory::NoiseCalcualtorPtr> m_noiseCalculatorPtrs;
  std::vector<NoiseCalculatorFactory::NoiseCalcualtorPtr> m_noise_beforeCMS_CalculatorPtrs;

  std::vector<CMSubtractorPtr> m_CMSEnginePtrs;  
};
} // namespace UTEmu
